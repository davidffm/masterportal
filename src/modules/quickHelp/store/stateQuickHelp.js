
/**
 * User type definition
 * @typedef {Object} QuickHelpState
 * @property {Boolean} active if true, quickhelp is rendered
 */
const state = {
    active: false
};

export default state;
